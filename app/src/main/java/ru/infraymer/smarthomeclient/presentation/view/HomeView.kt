package ru.infraymer.smarthomeclient.presentation.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.infraymer.smarthomeclient.entity.Room
import ru.infraymer.smarthomeclient.entity.Subsystem

@StateStrategyType(AddToEndSingleStrategy::class)
interface HomeView : MvpView {

    fun setSubsystems(subsystems: List<Subsystem>)
    fun setRooms(rooms: List<Room>)
}