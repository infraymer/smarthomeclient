package ru.infraymer.smarthomeclient.presentation.view

import com.arellomobile.mvp.MvpView
import ru.infraymer.smarthomeclient.entity.CallFunction
import ru.infraymer.smarthomeclient.entity.Device

interface RoomView : MvpView {

    fun setData(devices: List<Device>)
    fun setDevice(device: Device)
    fun setFunction(function: CallFunction)
}