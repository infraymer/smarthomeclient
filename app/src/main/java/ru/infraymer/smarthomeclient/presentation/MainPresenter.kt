package ru.infraymer.smarthomeclient.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import ru.infraymer.smarthomeclient.model.server.State
import ru.infraymer.smarthomeclient.model.server.Wamp
import ru.infraymer.smarthomeclient.system.Screens
import ru.terrakok.cicerone.Router

@InjectViewState
class MainPresenter(
    private val router: Router,
    private val wamp: Wamp
) : BasePresenter<MvpView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        wamp.state.subscribe { if (it == State.OPEN) router.newRootScreen(Screens.HOME) }
    }
}