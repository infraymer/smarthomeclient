package ru.infraymer.smarthomeclient.presentation.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.infraymer.smarthomeclient.entity.CallFunction

@StateStrategyType(AddToEndSingleStrategy::class)
interface DeviceView : MvpView {

    fun setData(funcLite: CallFunction)
}