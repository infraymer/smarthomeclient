package ru.infraymer.smarthomeclient.presentation

import com.arellomobile.mvp.InjectViewState
import ru.infraymer.smarthomeclient.model.server.SmartServer
import ru.infraymer.smarthomeclient.model.server.Wamp
import ru.infraymer.smarthomeclient.model.server.rx.RxState
import ru.infraymer.smarthomeclient.presentation.view.AuthView
import ru.infraymer.smarthomeclient.system.SchedulersProvider
import ru.infraymer.smarthomeclient.system.Screens
import ru.terrakok.cicerone.Router

@InjectViewState
class AuthPresenter(
    private val router: Router,
    private val server: SmartServer,
    private val scheduler: SchedulersProvider
) : BasePresenter<AuthView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
    }

    fun onSignInClicked(login: String, password: String) {

        server.connect(login, password)
            ?.doOnSubscribe { viewState.showProgress() }
            ?.doFinally { viewState.showProgress(false) }
            ?.subscribe(
                {
                    if (it is RxState && it.state == RxState.OPEN) router.navigateTo(Screens.HOME)
                },
                {
                    router.showSystemMessage(it.message)
                }
            )
    }

    fun onSettingsClicked() = router.navigateTo(Screens.SETTINGS)
}