package ru.infraymer.smarthomeclient.presentation

import com.arellomobile.mvp.InjectViewState
import ru.infraymer.smarthomeclient.entity.CallFunction
import ru.infraymer.smarthomeclient.model.repository.DeviceRepository
import ru.infraymer.smarthomeclient.presentation.view.DeviceView
import ru.terrakok.cicerone.Router

@InjectViewState
class DevicePresenter(
    private val deviceId: Int,
    private val router: Router,
    private val deviceRepository: DeviceRepository
) : BasePresenter<DeviceView>() {

    init {

    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

    }

    fun setParam(data: CallFunction) {

    }
}