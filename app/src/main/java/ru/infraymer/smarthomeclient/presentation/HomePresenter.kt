package ru.infraymer.smarthomeclient.presentation

import com.arellomobile.mvp.InjectViewState
import ru.infraymer.smarthomeclient.entity.Subsystem
import ru.infraymer.smarthomeclient.model.repository.DeviceRepository
import ru.infraymer.smarthomeclient.presentation.view.HomeView
import ru.terrakok.cicerone.Router

@InjectViewState
class HomePresenter(
    private val router: Router,
    private val deviceRepository: DeviceRepository
) : BasePresenter<HomeView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        deviceRepository.getDevices().subscribe(
            { viewState.setRooms(deviceRepository.rooms) },
            { router.showSystemMessage(it.message) }
        )
    }

    fun subsystemSelected(subsystem: Subsystem) {
    }
}