package ru.infraymer.smarthomeclient.presentation

import com.arellomobile.mvp.InjectViewState
import ru.infraymer.smarthomeclient.entity.CallFunction
import ru.infraymer.smarthomeclient.model.repository.DeviceRepository
import ru.infraymer.smarthomeclient.presentation.view.RoomView
import ru.terrakok.cicerone.Router

@InjectViewState
class RoomPresenter(
    private val roomId: Int,
    private val router: Router,
    private val deviceRepository: DeviceRepository
) : BasePresenter<RoomView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.setData(deviceRepository.getDevicesByRoom(roomId))
        deviceRepository.stateDeviceChange()
            .subscribe { viewState.setFunction(it) }
    }

    fun callFunction(data: CallFunction) {
        deviceRepository.callFunctionDevice(data).subscribe(
            { },
            { router.showSystemMessage(it.message) }
        )
    }


}