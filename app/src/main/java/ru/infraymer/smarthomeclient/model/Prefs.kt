package ru.infraymer.smarthomeclient.model

import android.content.Context
import org.jetbrains.anko.defaultSharedPreferences

class Prefs(
    private val context: Context
) : Settings {

    private val SERVER_DATA = "settings"
    private val SERVER_ADDRESS = "websocket_address"

    private fun getSharedPreferences(prefsName: String) =
        context.defaultSharedPreferences

    override var serverAddress: String
        get() = getSharedPreferences(SERVER_DATA).getString(SERVER_ADDRESS, "")
        set(value) {
            getSharedPreferences(SERVER_DATA).edit().putString(SERVER_ADDRESS, value).apply()
        }
}