package ru.infraymer.smarthomeclient.model.repository

import com.google.gson.Gson
import ru.infraymer.smarthomeclient.entity.CallFunction
import ru.infraymer.smarthomeclient.entity.Device
import ru.infraymer.smarthomeclient.entity.Room
import ru.infraymer.smarthomeclient.entity.Subsystem
import ru.infraymer.smarthomeclient.model.server.Wamp
import ru.infraymer.smarthomeclient.system.SchedulersProvider

class DeviceRepository(
    private val wamp: Wamp,
    private val schedulers: SchedulersProvider,
    private val gson: Gson
) {

    var susystems = listOf<Subsystem>()
        private set
    var devices = arrayListOf<Device>()
        private set
    var rooms = listOf<Room>()
        get() {
            val rooms = arrayListOf<Room>()
            devices.forEach { rooms.add(it.room) }
            rooms.sortBy { it.id }
            return rooms.distinct()
        }

    fun callFunctionDevice(data: CallFunction) =
        wamp.setParamDevice(data.idDevice, data.idFunction, data.value)
            .map { updateDevice(data) }

    fun getDevices() = wamp.getDevices().map { it.apply { devices = it } }

    fun getDevicesByRoom(roomId: Int): List<Device> =
        devices.filter { it.room.id == roomId }

    fun stateDeviceChange() = wamp.stateFunctionChange()
        .retryWhen { it }
        .map { updateDevice(it) }

    private fun updateDevice(data: CallFunction): CallFunction {
        val device = devices.find { it.id == data.idDevice }
        val function = device?.functions?.find { it.id == data.idFunction }
        function?.value = data.value
        return data
    }
}