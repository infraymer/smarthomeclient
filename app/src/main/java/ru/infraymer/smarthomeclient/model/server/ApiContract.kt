package ru.infraymer.smarthomeclient.model.server

object ApiContract {
    // IN
    const val DEVICE_INFO = "deviceInfo"
    const val DEVICES_INFO = "devicesInfo"
    const val SUBSYSTEMS_INFO = "subsystemsInfo"
    // OUT
    const val CALL_FUNCTION_DEVICE = "callFunctionDevice"
}