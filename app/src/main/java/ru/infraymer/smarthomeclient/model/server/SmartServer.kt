package ru.infraymer.smarthomeclient.model.server

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.ReplayRelay
import com.neovisionaries.ws.client.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.infraymer.smarthomeclient.model.Prefs
import ru.infraymer.smarthomeclient.model.Settings
import ru.infraymer.smarthomeclient.model.server.rx.RxError
import ru.infraymer.smarthomeclient.model.server.rx.RxEvent
import ru.infraymer.smarthomeclient.model.server.rx.RxMessage
import ru.infraymer.smarthomeclient.model.server.rx.RxState
import ru.infraymer.smarthomeclient.model.server.rx.RxState.Companion.CLOSED
import ru.infraymer.smarthomeclient.model.server.rx.RxState.Companion.CLOSING
import ru.infraymer.smarthomeclient.model.server.rx.RxState.Companion.CONNECTING
import ru.infraymer.smarthomeclient.model.server.rx.RxState.Companion.OPEN
import ru.infraymer.smarthomeclient.system.SchedulersProvider
import ru.infraymer.smarthomeclient.system.debug


class SmartServer(
    private val settings: Settings,
    private val scheduler: SchedulersProvider
) {

    var rxWebSocket: Observable<RxEvent>? = null
        private set
    private var webSocket: WebSocket? = null
    private val messageRelay = ReplayRelay.create<String>()
    private val stateRelay = PublishRelay.create<State>()
    val message: Observable<String> = messageRelay
    val state: Observable<State> = stateRelay

    private var login = ""
    private var password = ""

    var statusClosed = 0

    init {
        debug("INIT")
    }

    private fun createWebSocket() = Observable.create<RxEvent> {
        try {
            val address = settings.serverAddress
            if (address.isEmpty()) it.onError(Exception("Address of server is unknown"))
            webSocket?.disconnect()
            webSocket = null
            webSocket = WebSocketFactory()
                .setConnectionTimeout(3000)
                .createSocket(address)
                .addHeader("auth", "$login:$password")
            webSocket?.addListener(object : WebSocketAdapter() {

                override fun onTextMessage(websocket: WebSocket, text: String) {
                    debug("WebSocket - MESSAGE - $text")
                    it.onNext(RxMessage(text))
                }

                override fun onStateChanged(ws: WebSocket, newState: WebSocketState) {
                    debug("WebSocket - STATE - $newState")
                    when (newState) {
                        WebSocketState.OPEN -> it.onNext(RxState(OPEN))
                        WebSocketState.CONNECTING -> it.onNext(RxState(CONNECTING))
                        WebSocketState.CLOSING -> it.onNext(RxState(CLOSING))
                        WebSocketState.CLOSED -> it.onNext(RxState(CLOSED))
                    }
                }

                override fun onConnected(
                    websocket: WebSocket, headers: MutableMap<String, MutableList<String>>) {
                    debug("WebSocket - CONNECTED - onConnected")
                }

                override fun onDisconnected(websocket: WebSocket, serverCloseFrame: WebSocketFrame,
                                            clientCloseFrame: WebSocketFrame, closedByServer: Boolean) {
                    debug("WebSocket - DISCONNECTED - closedByServer=$closedByServer")
                }
            })
            webSocket?.connect()
        } catch (e: OpeningHandshakeException) {
            it.onError(e)
        } catch (e: HostnameUnverifiedException) {
            it.onError(e)
        } catch (e: WebSocketException) {
            it.onError(e)
        }
    }

    fun subscribeEvents(
        rxMessage: ((rxMessage: RxMessage) -> Unit) = {},
        rxState: ((rxState: RxState) -> Unit) = {},
        rxError: ((rxError: RxError) -> Unit) = {},
        onError: ((t: Throwable) -> Unit) = {}
    ) = rxWebSocket?.subscribe(
        {
            when (it) {
                is RxMessage -> rxMessage(it)
                is RxError -> rxError(it)
                is RxState -> rxState(it)
            }
        },
        { onError(it) }
    )

    fun connect(login: String, password: String): Observable<RxEvent>? {
        this.login = login
        this.password = password
        rxWebSocket = createWebSocket()
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
        return rxWebSocket
    }

    fun disconnect() {
        webSocket?.disconnect()
    }

    fun reconnect() {
        webSocket = webSocket?.recreate()?.connectAsynchronously()
    }

    fun send(text: String) = Observable.fromCallable { webSocket?.sendText(text) }

}