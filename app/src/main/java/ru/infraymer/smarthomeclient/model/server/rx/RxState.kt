package ru.infraymer.smarthomeclient.model.server.rx

class RxState(val state: Int) : RxEvent() {

    companion object {
        const val CONNECTING = 0
        const val OPEN = 1
        const val CLOSING = 2
        const val CLOSED = 3
    }
}