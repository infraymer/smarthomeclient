package ru.infraymer.smarthomeclient.model.server

enum class State {
    CONNECTING, OPEN, CLOSING, CLOSED
}