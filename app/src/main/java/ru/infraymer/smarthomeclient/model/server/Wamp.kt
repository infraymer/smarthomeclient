package ru.infraymer.smarthomeclient.model.server

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.jakewharton.rxrelay2.BehaviorRelay
import hu.akarnokd.rxjava.interop.RxJavaInterop
import io.reactivex.Observable
import ru.infraymer.smarthomeclient.entity.CallFunction
import ru.infraymer.smarthomeclient.entity.Device
import ru.infraymer.smarthomeclient.system.SchedulersProvider
import ws.wamp.jawampa.SubscriptionFlags
import ws.wamp.jawampa.WampClient
import ws.wamp.jawampa.WampClientBuilder
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider
import java.util.concurrent.TimeUnit

class Wamp(
    private val gson: Gson,
    private val scheduler: SchedulersProvider
) {

    private lateinit var client: WampClient
    private val stateRelay = BehaviorRelay.create<State>()
    val state: Observable<State> = stateRelay

    init {
        try {
            val builder = WampClientBuilder().apply {
                withConnectorProvider(NettyWampClientConnectorProvider())
                withUri("ws://192.168.1.62:50100/ws")
                withRealm("smart")
                withInfiniteReconnects()
                withReconnectInterval(3, TimeUnit.SECONDS)
            }
            client = builder.build()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        RxJavaInterop.toV2Observable(client.statusChanged())
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .subscribe(
                { state ->
                    when (state) {
                        is WampClient.ConnectedState -> this.stateRelay.accept(State.OPEN)
                        is WampClient.ConnectingState -> this.stateRelay.accept(State.CONNECTING)
                        is WampClient.DisconnectedState -> this.stateRelay.accept(State.CLOSED)
                    }
                },
                { it.printStackTrace() }
            )
        client.open()
    }

    fun getDevices() = RxJavaInterop
        .toV2Observable(client.call("smart.subs.get_devices", String, null))
        .subscribeOn(scheduler.io())
        .observeOn(scheduler.ui())
        .map { gson.fromJson<ArrayList<Device>>(it.arguments()[0].toString()) }

    fun setParamDevice(idDevice: Int, idFunction: Int, value: Int) = RxJavaInterop
        .toV2Observable(client.call(
            "smart.subs.set_param_device", String,
            idDevice, idFunction, value))
        .subscribeOn(scheduler.io())
        .observeOn(scheduler.ui())

    fun stateFunctionChange() = RxJavaInterop
        .toV2Observable(client.makeSubscription("smart.subs.function", SubscriptionFlags.Exact, String::class.java))
        .subscribeOn(scheduler.io())
        .observeOn(scheduler.ui())
        .map { gson.fromJson<CallFunction>(it) }
}