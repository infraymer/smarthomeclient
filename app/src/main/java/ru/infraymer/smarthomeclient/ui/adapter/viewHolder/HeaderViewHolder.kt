package ru.infraymer.smarthomeclient.ui.adapter.viewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import org.jetbrains.anko.find
import ru.infraymer.smarthomeclient.R
import ru.infraymer.smarthomeclient.entity.Category

class HeaderViewHolder(
    view: View
) : RecyclerView.ViewHolder(view) {

    private val name = view.find<TextView>(R.id.nameTextView)

    fun bind(category: Category) {
        name.text = category.name
    }
}