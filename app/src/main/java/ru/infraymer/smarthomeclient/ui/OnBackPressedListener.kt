package ru.infraymer.smarthomeclient.ui

interface OnBackPressedListener {

    fun onBackPressed()
}