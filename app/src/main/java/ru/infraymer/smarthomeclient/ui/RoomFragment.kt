package ru.infraymer.smarthomeclient.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_room.*
import org.koin.android.ext.android.get
import ru.infraymer.smarthomeclient.R
import ru.infraymer.smarthomeclient.di.Params
import ru.infraymer.smarthomeclient.entity.CallFunction
import ru.infraymer.smarthomeclient.entity.Device
import ru.infraymer.smarthomeclient.presentation.RoomPresenter
import ru.infraymer.smarthomeclient.presentation.view.RoomView
import ru.infraymer.smarthomeclient.ui.adapter.DeviceAdapter

class RoomFragment : BaseFragment(), RoomView {

    companion object {
        private const val ARG_ROOM_ID = "room_id"
        fun newInstance(roomId: Int) = RoomFragment().apply {
            arguments = Bundle().apply {
                putInt(ARG_ROOM_ID, roomId)
            }
        }
    }

    override val layoutRes = R.layout.fragment_room

    @ProvidePresenter
//    fun providePresenter(): RoomPresenter = get { mapOf(Params.ROOM_ID to arguments?.getInt(ARG_ROOM_ID)!!) }
    fun providePresenter(): RoomPresenter = RoomPresenter(arguments?.getInt(ARG_ROOM_ID)!!, get(), get())

    private val deviceAdapter by lazy {
        DeviceAdapter { presenter.callFunction(it) }
    }

    @InjectPresenter
    lateinit var presenter: RoomPresenter

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = deviceAdapter
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        }
    }

    override fun onBackPressed() {}

    override fun setData(devices: List<Device>) {
        deviceAdapter.data.apply {
            clear()
            add(devices[0].category)
            add(devices[0])
            for (i in 1 until devices.size) {
                if (devices[i].category.id != devices[i - 1].category.id) {
                    add(devices[i].category)
                    add(devices[i])
                } else {
                    add(devices[i])
                }
            }
        }
        deviceAdapter.notifyDataSetChanged()
    }

    override fun setDevice(device: Device) {
        /*val index = deviceAdapter.data.indexOfFirst { it.id == device.id }
        deviceAdapter.data.set(index, device)
        deviceAdapter.notifyItemChanged(index)*/
    }

    override fun setFunction(function: CallFunction) {
        deviceAdapter.updateFunction(function)
    }
}