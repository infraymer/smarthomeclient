package ru.infraymer.smarthomeclient.ui

import android.os.Bundle
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewCompat
import android.widget.TextView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.github.zawadz88.materialpopupmenu.MaterialPopupMenu
import com.github.zawadz88.materialpopupmenu.popupMenu
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.get
import ru.infraymer.smarthomeclient.R
import ru.infraymer.smarthomeclient.entity.Room
import ru.infraymer.smarthomeclient.entity.Subsystem
import ru.infraymer.smarthomeclient.presentation.HomePresenter
import ru.infraymer.smarthomeclient.presentation.view.HomeView
import ru.infraymer.smarthomeclient.system.visible

class HomeFragment : BaseFragment(), HomeView {

    override val layoutRes = R.layout.fragment_home

    @InjectPresenter
    lateinit var presenter: HomePresenter

    @ProvidePresenter
    fun providePresenter(): HomePresenter = get()

    private var popupSubsystem: MaterialPopupMenu? = null

    override fun onBackPressed() {}

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewCompat.setElevation(toolbar, 0f)
        title.setOnClickListener {
            popupSubsystem?.show(context!!, title)
        }
    }

    override fun setRooms(rooms: List<Room>) {
        viewPager.adapter = RoomAdapter(rooms)
    }

    override fun setSubsystems(subsystems: List<Subsystem>) {
        if (subsystems.isNotEmpty()) {
            title.text = subsystems[0].name
            popupSubsystem = popupMenu {
                section {
                    subsystems.forEach {
                        customItem {
                            layoutResId = R.layout.item_subsystem
                            viewBoundCallback = { view ->
                                val nameTextView: TextView = view.findViewById(R.id.nameTextView)
                                val onlineTextView: TextView = view.findViewById(R.id.onlineTextView)
                                nameTextView.text = it.name
                                onlineTextView.visible(it.online)
                            }
                            callback = {
                                this@HomeFragment.title.text = it.name
                                presenter.subsystemSelected(it)
                            }
                        }
                    }
                }
            }
        }
    }

    private inner class RoomAdapter(
        private val rooms: List<Room>
    ) : FragmentStatePagerAdapter(childFragmentManager) {

        override fun getItem(position: Int) = RoomFragment.newInstance(rooms[position].id)

        override fun getCount() = rooms.size

        override fun getPageTitle(position: Int) = rooms[position].name
    }
}