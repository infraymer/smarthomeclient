package ru.infraymer.smarthomeclient.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import ru.infraymer.smarthomeclient.R
import ru.infraymer.smarthomeclient.entity.CallFunction
import ru.infraymer.smarthomeclient.entity.Category
import ru.infraymer.smarthomeclient.entity.Device
import ru.infraymer.smarthomeclient.system.inflate
import ru.infraymer.smarthomeclient.ui.adapter.viewHolder.DeviceViewHolder
import ru.infraymer.smarthomeclient.ui.adapter.viewHolder.HeaderViewHolder

class DeviceAdapter(
    private val callFunction: (data: CallFunction) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_CATEGORY = 0
        private const val TYPE_DEVICE = 1
    }

    var data = arrayListOf<Any>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    fun updateFunction(func: CallFunction) {
        data.find { if (it is Device) it.id == func.idDevice else false }?.let {
            val device = (it as Device)
            device.functions.find { it.id == func.idFunction }?.apply {
                value = func.value
                notifyItemChanged(data.indexOf(device), func)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_DEVICE -> DeviceViewHolder(parent.inflate(R.layout.item_device), callFunction)
            TYPE_CATEGORY -> HeaderViewHolder(parent.inflate(R.layout.item_category))
            else -> throw RuntimeException()
        }
    }

    override fun getItemCount() = data.size

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is Category -> TYPE_CATEGORY
            else -> TYPE_DEVICE
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            TYPE_CATEGORY -> (holder as HeaderViewHolder).bind(data[position] as Category)
            TYPE_DEVICE -> (holder as DeviceViewHolder).bind(data[position] as Device)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)
        if (payloads.isNotEmpty()) (holder as? DeviceViewHolder)?.updateData(payloads[0] as CallFunction)
    }
}