package ru.infraymer.smarthomeclient.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import ru.infraymer.smarthomeclient.R
import ru.infraymer.smarthomeclient.presentation.MainPresenter
import ru.infraymer.smarthomeclient.system.Screens
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportFragmentNavigator

class MainActivity : MvpAppCompatActivity(), MvpView {

    @InjectPresenter
    lateinit var presenter: MainPresenter

    @ProvidePresenter
    fun providePresenter(): MainPresenter = get()

    private val navigatorHolder: NavigatorHolder by inject()

    private val currentFragment
        get() = supportFragmentManager.findFragmentById(R.id.container) as OnBackPressedListener?

    private val navigator = object : SupportFragmentNavigator(supportFragmentManager, R.id.container) {

        override fun exit() {
            finishAffinity()
        }

        override fun createFragment(screenKey: String, data: Any?): Fragment {
            return when (screenKey) {
                Screens.AUTH -> AuthFragment()
                Screens.SETTINGS -> SettingsFragment()
                Screens.HOME -> HomeFragment()
                else -> throw RuntimeException("Unknown screen")
            }
        }

        override fun showSystemMessage(message: String) {
            toast(message)
        }
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }
}
