package ru.infraymer.smarthomeclient.ui

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_auth.*
import org.koin.android.ext.android.get
import ru.infraymer.smarthomeclient.R
import ru.infraymer.smarthomeclient.presentation.AuthPresenter
import ru.infraymer.smarthomeclient.presentation.view.AuthView
import ru.infraymer.smarthomeclient.system.visible

class AuthFragment : BaseFragment(), AuthView {

    override val layoutRes = R.layout.fragment_auth

    @InjectPresenter
    lateinit var presenter: AuthPresenter

    @ProvidePresenter
    fun providePesenter(): AuthPresenter = get()

    private val login: String
        get() = loginEditText.text.toString()
    private val password: String
        get() = passwordEditText.text.toString()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        signInButton.setOnClickListener { presenter.onSignInClicked(login, password) }
        settingsButton.setOnClickListener { presenter.onSettingsClicked() }
    }

    override fun onBackPressed() {}

    override fun showProgress(show: Boolean) {
        progress.visible(show, false)
        signInButton.isEnabled = !show
    }
}