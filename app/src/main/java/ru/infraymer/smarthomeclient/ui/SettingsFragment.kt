package ru.infraymer.smarthomeclient.ui

import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.koin.android.ext.android.inject
import ru.infraymer.smarthomeclient.R
import ru.infraymer.smarthomeclient.system.Screens
import ru.terrakok.cicerone.Router

class SettingsFragment : PreferenceFragmentCompat(), OnBackPressedListener {


    private val router: Router by inject()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.settings)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val toolbar = LayoutInflater.from(view.context).inflate(R.layout.layout_toolbar, view as ViewGroup)
//        view.addView(toolbar, 0)

    }

    override fun onBackPressed() = router.backTo(Screens.AUTH)
}