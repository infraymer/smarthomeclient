package ru.infraymer.smarthomeclient.ui.adapter.viewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar
import org.jetbrains.anko.find
import ru.infraymer.smarthomeclient.R
import ru.infraymer.smarthomeclient.entity.CallFunction
import ru.infraymer.smarthomeclient.entity.Device
import ru.infraymer.smarthomeclient.entity.Function
import ru.infraymer.smarthomeclient.entity.FunctionLite
import ru.infraymer.smarthomeclient.system.inflate
import kotlin.collections.set

class DeviceViewHolder(
    private val view: View,
    private val setParam: (data: CallFunction) -> Unit
) : RecyclerView.ViewHolder(view) {

    lateinit var device: Device
    private val params = mutableMapOf<Int, ParamHolder>()

    private val name = view.find<TextView>(R.id.nameTextView)
    private val functionContainer = view.find<LinearLayout>(R.id.functionContainerViewGroup)

    fun bind(device: Device) {
        this.device = device
        functionContainer.removeAllViews()
        name.text = device.name
        if (device.functions.isEmpty()) view.alpha = 0.3f
        device.functions.forEach {
            if (it.minValue == 0 && it.maxValue == 1)
                params[it.id] = SwitchHolder(functionContainer, it)
            else
                params[it.id] = SliderHolder(functionContainer, it)
        }
    }

    fun updateData(funcLite: CallFunction) {
        device.functions.find { it.id == funcLite.idFunction }?.apply {
            value = funcLite.value
            params[id]?.setValue(value)
        }
    }

    private abstract class ParamHolder {
        abstract fun setValue(value: Any)
    }

    private inner class SwitchHolder(parent: ViewGroup, function: Function) : ParamHolder() {

        private val view = parent.inflate(R.layout.layout_switch_function, true)
        private var switch: Switch

        init {
            switch = view.find(R.id.trigger)
            switch.text = function.name
            switch.isChecked = function.value != 0
            switch.setOnCheckedChangeListener { _, isChecked ->
                val value = if (isChecked) 1 else 0
                //functionListener(CallFunction(device.id, function.id, value))
                setParam(CallFunction(device.id, function.id, value))
            }
        }

        override fun setValue(value: Any) {
            switch.isChecked = value as Int == 1
        }
    }

    private inner class SliderHolder(parent: ViewGroup, function: Function) : ParamHolder() {

        private val view = parent.inflate(R.layout.layout_slider_function, true)
        private var seekBar: DiscreteSeekBar
        private var valueTextView: TextView

        init {
            val nameTextView: TextView = view.find(R.id.nameTextView)
            nameTextView.text = function.name
            seekBar = view.find(R.id.seekBar)
            seekBar.min = function.minValue
            seekBar.max = function.maxValue
            seekBar.progress = function.value
            val minTextView: TextView = view.find(R.id.minValueTextView)
            minTextView.text = function.minValue.toString()
            val maxTextView: TextView = view.find(R.id.maxValueTextView)
            maxTextView.text = function.maxValue.toString()
            valueTextView = view.find(R.id.valueTextView)
            valueTextView.text = function.value.toString()
            seekBar.setOnProgressChangeListener(object : DiscreteSeekBar.OnProgressChangeListener {
                var startValue = 0
                override fun onProgressChanged(seekBar: DiscreteSeekBar, value: Int, fromUser: Boolean) {
                    if (startValue != seekBar.progress) {
                        valueTextView.text = seekBar.progress.toString()
//                    functionListener(CallFunction(device.id, function.id, seekBar.progress))
                        setParam(CallFunction(device.id, function.id, value))
                    }
                }

                override fun onStartTrackingTouch(seekBar: DiscreteSeekBar) {
//                startValue = seekBar.progress
                }

                override fun onStopTrackingTouch(seekBar: DiscreteSeekBar) {

                }
            })
        }

        override fun setValue(value: Any) {
            seekBar.progress = value as Int
            valueTextView.text = value.toString()
        }
    }
}