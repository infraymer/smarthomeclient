package ru.infraymer.smarthomeclient.di

import android.content.Context
import com.google.gson.Gson
import org.koin.dsl.module.applicationContext
import ru.infraymer.smarthomeclient.di.Params.DEVICE_ID
import ru.infraymer.smarthomeclient.di.Params.ROOM_ID
import ru.infraymer.smarthomeclient.model.Prefs
import ru.infraymer.smarthomeclient.model.Settings
import ru.infraymer.smarthomeclient.model.repository.DeviceRepository
import ru.infraymer.smarthomeclient.model.server.SmartServer
import ru.infraymer.smarthomeclient.model.server.Wamp
import ru.infraymer.smarthomeclient.presentation.*
import ru.infraymer.smarthomeclient.system.AppSchedulers
import ru.infraymer.smarthomeclient.system.SchedulersProvider
import ru.terrakok.cicerone.Cicerone

fun appModule(context: Context) = applicationContext {
    bean { AppSchedulers() as SchedulersProvider }
    bean { Gson() }
    bean { Prefs(context) as Settings }

    val cicerone = Cicerone.create()
    bean { cicerone.router }
    bean { cicerone.navigatorHolder }

    bean { Wamp(get(), get()) }
    bean { SmartServer(get(), get()) }

    bean { DeviceRepository(get(), get(), get()) }

    factory { MainPresenter(get(), get()) }
    factory { HomePresenter(get(), get()) }
    factory { AuthPresenter(get(), get(), get()) }
//    factory { params -> RoomPresenter(params[ROOM_ID], get(), get()) }
    factory { params -> DevicePresenter(params[DEVICE_ID], get(), get()) }

}


object Params {
    const val ROOM_ID = "ROOM_ID"
    const val DEVICE_ID = "DEVICE_ID"
}