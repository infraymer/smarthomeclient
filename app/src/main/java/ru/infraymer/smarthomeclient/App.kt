package ru.infraymer.smarthomeclient

import android.app.Application
import org.koin.android.ext.android.startKoin
import ru.infraymer.smarthomeclient.di.appModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule(applicationContext)))
        //startKoin(this, listOf(testModule()))

    }
}