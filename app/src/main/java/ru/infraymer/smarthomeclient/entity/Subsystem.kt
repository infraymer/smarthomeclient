package ru.infraymer.smarthomeclient.entity

data class Subsystem(
    val id: Int,
    val name: String,
    val online: Boolean
)