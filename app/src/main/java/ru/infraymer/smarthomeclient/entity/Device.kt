package ru.infraymer.smarthomeclient.entity

data class Device(
    val id: Int,
    val name: String,
    val room: Room,
    val category: Category,
    val functions: List<Function>
)