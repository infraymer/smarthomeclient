package ru.infraymer.smarthomeclient.entity

data class Category(
    val id: Int,
    val name: String
)