package ru.infraymer.smarthomeclient.entity

data class FunctionLite(
    val idDevice: Int,
    val idFunction: Int,
    val value: Int
)