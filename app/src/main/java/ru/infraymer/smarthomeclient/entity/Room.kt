package ru.infraymer.smarthomeclient.entity

data class Room(
    val id: Int,
    val name: String
)