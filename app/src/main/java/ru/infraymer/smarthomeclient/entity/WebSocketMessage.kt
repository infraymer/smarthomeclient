package ru.infraymer.smarthomeclient.entity

import com.google.gson.JsonElement

data class WebSocketMessage(
    val action: String,
    val data: JsonElement?,
    val error: JsonElement?
)