package ru.infraymer.smarthomeclient.entity

data class Function(
    var id: Int,
    var name: String,
    var value: Int,
    var isWriteEnable: Boolean,
    var minValue: Int,
    var maxValue: Int
)