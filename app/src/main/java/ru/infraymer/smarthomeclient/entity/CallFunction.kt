package ru.infraymer.smarthomeclient.entity

data class CallFunction(
    val idDevice: Int,
    val idFunction: Int,
    val value: Int
)