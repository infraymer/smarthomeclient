package ru.infraymer.smarthomeclient.system

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun Context.color(colorRes: Int) = ContextCompat.getColor(this, colorRes)

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun View.visible(show: Boolean = true, gone: Boolean = true) {
    visibility = if (show) View.VISIBLE else if (gone) View.GONE else View.INVISIBLE
}

fun Any.debug(text: String) = ""
fun Any.err(text: String) = Log.e(javaClass.simpleName, text)