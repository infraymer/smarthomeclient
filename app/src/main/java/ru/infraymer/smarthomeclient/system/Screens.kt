package ru.infraymer.smarthomeclient.system

object Screens {

    const val AUTH = "auth"
    const val HOME = "home"
    const val SETTINGS = "settings"
}