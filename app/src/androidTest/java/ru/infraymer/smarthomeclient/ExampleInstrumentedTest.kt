package ru.infraymer.smarthomeclient

import android.support.test.runner.AndroidJUnit4
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import ru.infraymer.smarthomeclient.di.Params
import ru.infraymer.smarthomeclient.entity.CallFunction
import ru.infraymer.smarthomeclient.entity.WebSocketMessage
import ru.infraymer.smarthomeclient.model.Settings
import ru.infraymer.smarthomeclient.model.repository.DeviceRepository
import ru.infraymer.smarthomeclient.model.server.ApiContract
import ru.infraymer.smarthomeclient.model.server.SmartServer
import ru.infraymer.smarthomeclient.model.server.rx.RxMessage
import ru.infraymer.smarthomeclient.model.server.rx.RxState
import ru.infraymer.smarthomeclient.presentation.AuthPresenter
import ru.infraymer.smarthomeclient.presentation.HomePresenter
import ru.infraymer.smarthomeclient.presentation.MainPresenter
import ru.infraymer.smarthomeclient.presentation.RoomPresenter
import ru.infraymer.smarthomeclient.system.SchedulersProvider
import ru.terrakok.cicerone.Cicerone


@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest : KoinComponent {

    val login = "danay"
    val password = "qwe123"

    val gson: Gson by inject()
    val schduler: TestScheduler by inject()
    val smartServer: SmartServer by inject()

    // Тест на авторизацию на сервере
    @Test
    fun auth() {
        smartServer.connect(login, password)?.subscribe {
            if (it is RxState && it.state == RxState.OPEN) Assert.assertTrue(true)
            else if (it is RxState && it.state == RxState.CLOSED) Assert.assertTrue(false)
        }
        schduler.triggerActions()
    }

    // Тест на установку нового параметра функции устройства
    @Test
    fun callFunction() {
        val message = WebSocketMessage(
            ApiContract.CALL_FUNCTION_DEVICE,
            gson.toJsonTree(CallFunction(55, 5, 1)),
            null
        )
        smartServer.connect(login, password)?.subscribe {
            if (it is RxState && it.state == RxState.OPEN) {
                smartServer.send(gson.toJson(message)).subscribe()
            }
            if (it is RxMessage) {
                val msg = gson.fromJson<WebSocketMessage>(it.message)
                if (msg.action == ApiContract.DEVICE_INFO) {
                    Assert.assertNull(msg.error)
                }
            }
        }
        schduler.triggerActions()
    }

    // Тест на получение информации о подсистемах при подключении к серверу
    @Test
    fun subsystemsInfo() {
        smartServer.connect(login, password)?.subscribe {
            if (it is RxMessage) {
                val msg = gson.fromJson<WebSocketMessage>(it.message)
                if (msg.action == ApiContract.SUBSYSTEMS_INFO) {
                    Assert.assertNotNull(msg.data)
                }
            }
        }
        schduler.triggerActions()
    }

    // Тестовый класс с настройками
    class TestSettings() : Settings {
        override var serverAddress: String
            get() = "ws://192.168.1.2:50100/client"
            set(value) {}
    }

    // Тестовый класс настройки потоков
    class TestSched(private val scheduler: TestScheduler) : SchedulersProvider {
        override fun ui() = scheduler
        override fun computation() = scheduler
        override fun trampoline() = scheduler
        override fun newThread() = scheduler
        override fun io() = scheduler
    }

    // Тестовый DI модуль
    fun testModule() = applicationContext {
        bean { TestScheduler() }
        bean { TestSched(get()) as SchedulersProvider }
        bean { Gson() }
        bean { TestSettings() as Settings }

        val cicerone = Cicerone.create()
        bean { cicerone.router }
        bean { cicerone.navigatorHolder }

        bean { SmartServer(get(), get()) }

        bean { DeviceRepository(get(), get(), get()) }

        factory { MainPresenter(get(), get()) }
        factory { HomePresenter(get(), get()) }
        factory { AuthPresenter(get(), get(), get()) }
        factory { params -> RoomPresenter(params[Params.ROOM_ID], get(), get()) }
    }
}