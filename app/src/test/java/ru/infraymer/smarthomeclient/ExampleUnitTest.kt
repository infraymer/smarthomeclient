package ru.infraymer.smarthomeclient

import com.google.gson.Gson
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.dsl.module.applicationContext
import org.koin.standalone.StandAloneContext.closeKoin
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest
import ru.infraymer.smarthomeclient.model.Settings
import ru.infraymer.smarthomeclient.model.server.SmartServer
import ru.infraymer.smarthomeclient.model.server.rx.RxState
import ru.infraymer.smarthomeclient.system.SchedulersProvider

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest : KoinTest {

    val smartServer: SmartServer by inject()

    @Before
    fun before() {
        startKoin(listOf(testModule()))
    }

    @After
    fun after() {
        closeKoin()
    }

    @Test
    fun connect() {
        smartServer.connect("danay", "qwe123")?.subscribe {
            if (it is RxState && it.state == RxState.OPEN) assert(true)
        }

    }

    class TestSettings() : Settings {
        override var serverAddress: String
            get() = "ws://192.168.1.2:50100/client"
            set(value) {}
    }

    class TestSched(private val scheduler: TestScheduler) : SchedulersProvider {
        override fun ui() = scheduler
        override fun computation() = scheduler
        override fun trampoline() = scheduler
        override fun newThread() = scheduler
        override fun io() = scheduler
    }

    fun testModule() = applicationContext {

        bean { TestScheduler() }
        bean { TestSched(get()) as SchedulersProvider }
        bean { Gson() }
        bean { TestSettings() as Settings }

        bean { SmartServer(get(), get()) }

    }
}




