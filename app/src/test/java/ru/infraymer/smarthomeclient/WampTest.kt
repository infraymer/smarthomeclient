package ru.infraymer.smarthomeclient

import hu.akarnokd.rxjava.interop.RxJavaInterop
import io.reactivex.schedulers.TestScheduler
import junit.framework.Assert
import org.junit.Before
import org.junit.Test
import ws.wamp.jawampa.WampClient
import ws.wamp.jawampa.WampClientBuilder
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider
import java.util.concurrent.TimeUnit

class WampTest {

    lateinit var scheduler: TestScheduler

    @Before
    fun before() {
        scheduler = TestScheduler()
    }

    @Test
    fun getDevices() {
        val client = WampClientBuilder().apply {
            withConnectorProvider(NettyWampClientConnectorProvider())
            withUri("ws://localhost:50100/ws")
            withRealm("smart")
            withInfiniteReconnects()
            withReconnectInterval(3, TimeUnit.SECONDS)
        }.build()
        RxJavaInterop.toV2Observable(client.statusChanged())
            .observeOn(scheduler)
            .subscribeOn(scheduler)
            .subscribe(
                { state ->
                    if (state is WampClient.ConnectedState) {
                        client.call("smart.subs.get_devices", String::class.java)
                            .subscribe(
                                {
                                    System.out.println(it)
                                    Assert.assertTrue(true)
                                },
                                {
                                    it.printStackTrace()
                                    Assert.assertTrue(false)
                                }
                            )
                    }
                },
                {
                    it.printStackTrace()
                    Assert.assertTrue(false)
                }
            )
        scheduler.triggerActions()
    }
}